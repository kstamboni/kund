<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/clientes/pj/{id}', 'ClienteController@apresenta_pj')->where('id', '[0-9]+');

Route::get('/clientes/pf/{id}', 'ClienteController@apresenta_pf')->where('id', '[0-9]+');

Route::get('/clientes/cadastrar', 'ClienteController@apresenta_cadastro');

Route::get('/clientes/listar', 'ClienteController@lista_todos');

Route::get('/clientes/json', 'ClienteController@lista_todos_json');

Route::get('/clientes/pj/{id}/editar', 'ClienteController@apresenta_edicao_pj')->where('id', '[0-9]+');

Route::get('/clientes/pf/{id}/editar', 'ClienteController@apresenta_edicao_pf')->where('id', '[0-9]+');

Route::post('/clientes', 'ClienteController@cadastra');

Route::post('/clientes/pj/{id}', 'ClienteController@edita_pj')->where('id', '[0-9]+');

Route::post('/clientes/pf/{id}', 'ClienteController@edita_pf')->where('id', '[0-9]+');

Route::delete('/clientes/ajax/', 'ClienteController@exclui')->where('id', '[0-9]+');

Route::post('/clientes/ajax/validar', 'ClienteController@verifica_conflito');
