<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pjs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->char('cnpj', 18)->unique();
            $table->string('razao', 100);
            $table->string('fantasia', 100);
            $table->integer('numero');
            $table->string('complemento', 100)->nullable();
            $table->char('cep', 8);
            $table->string('logradouro', 100);
            $table->string('bairro', 100);
            $table->string('cidade', 100);
            $table->string('uf', 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pjs');
    }
}
