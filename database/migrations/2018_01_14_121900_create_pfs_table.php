<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pfs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->char('cpf', 14)->unique();
            $table->date('nascimento');
            $table->string('nome', 100);
            $table->string('sobrenome', 15);
            $table->integer('numero');
            $table->string('complemento', 100)->nullable();
            $table->char('cep', 8);
            $table->string('logradouro', 100);
            $table->string('bairro', 100);
            $table->string('cidade', 100);
            $table->string('uf', 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pfs');
    }
}
