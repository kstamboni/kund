<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
          <div class="panel-body">
            <h1 class="display-2">Olá! Este é o Kund<small><sub>beta</sub></small></h1>
            <p>O Kund é uma plataforma para gerenciamento de clientes, sejam eles pessoas físicas ou jurídicas.</p>
            <hr>
            <div id="input-tipo-pessoa" class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Escolha o cadastro para pessoa física ou jurídica.">
              <div class="btn-group" role="group">
              <a href="/clientes/cadastrar"><button id="button-pf" type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Cadastrar cliente</button></a>
              </div>
              <div class="btn-group" role="group">
                <a href="/clientes/listar"><button id="button-pj" type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listar clientes cadastrados</button></a>
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
    </main>
  </div>
  @include('layout.footer')
</body>
</html>
