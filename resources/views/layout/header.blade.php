<header class="navbar navbar-default navbar-fixed-top header-color">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Barra de navegação</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/" title="Ir para a página inicial">KUND</a>
    </div>
    <nav class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="/clientes/cadastrar" title="Cadastrar um(a) novo(a) cliente"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Cadastrar cliente</a></li>
        <li><a href="/clientes/listar" title="Visualizar clientes cadastrados"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listar clientes cadastrados</a></li>
      </ul>
    </nav>
  </div>
</header>
