<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Listar clientes - Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <h1>Clientes registrados</h1>
        <div id="aviso" class="alert alert-success alert-dismissible hidden" style="background-color: white;" role="alert">
        </div>
        <div class="panel panel-default">
          <ul class="list-group">
            @forelse($pjs as $pj)
            <li class="list-group-item">
              <a href="/clientes/pj/{{ $pj->id }}" class="item-link" title="Visualizar"><strong>PJ</strong> <span class="item-text">{{ $pj->fantasia }}</span></a>
               <div class="pull-right action-buttons">
                  <a href="/clientes/pj/{{ $pj->id }}" class="edit-zoom" title="Visualizar"><span class="glyphicon glyphicon-search"></span></a>
                  <a href="/clientes/pj/{{ $pj->id }}/editar" class="edit-zoom" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                </div>
            </li>
            @empty
            <li class="list-group-item">
              <span class="list-group-item-static">Não há pessoas jurídicas registradas.</span>
            </li>
            @endforelse
            @forelse($pfs as $pf)
            <li class="list-group-item">
              <a href="/clientes/pf/{{ $pf->id }}" class="item-link" title="Visualizar"><strong>PF</strong> <span class="item-text">{{ $pf->nome }} {{ $pf->sobrenome }}</span></a>
               <div class="pull-right action-buttons">
                  <a href="/clientes/pf/{{ $pf->id }}" class="edit-zoom" title="Visualizar"><span class="glyphicon glyphicon-search"></span></a>
                  <a href="/clientes/pf/{{ $pf->id }}/editar" class="edit-zoom" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                </div>
            </li>
            @empty
            <li class="list-group-item">
              <span class="list-group-item-static">Não há pessoas físicas registradas.</span>
            </li>
            @endforelse
          </ul>
        </div>
      </div>
    </main>
  </div>
  @include('layout.footer')
  <script src="/js/listar-clientes.js"></script>
</body>
</html>
