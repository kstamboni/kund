<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Editar - Pessoa Jurídica - Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <h1>Editar cadastro</h1>
        <div class="panel panel-default">
          <div class="panel-body">
            <form id="cadastro-form" onsubmit="return cadastrar_pj()" action="/clientes/pj/{{ $pj->id }}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="tipo" value="PJ">
  					  <div class="form-group" id="input-cnpj-group">
  						  <label for="input-cnpj">CNPJ</label>
  						  <input type="text" id="input-cnpj" name="cnpj" class="form-control" placeholder="Ex: 71.526.721/0001-32" maxlength="18" value="{{ $pj->cnpj }}" disabled>
  						  <span class="help-block hidden" id="input-cnpj-error"></span>
  					  </div>
              <div class="form-group has-success" id="input-razao-group">
  					    <label for="input-razao">Razão social</label>
  					    <input type="text" id="input-razao" name="razao" class="form-control" placeholder="Ex: Belitalus Prime Transportes do Brasil S/A" maxlength="100" value="{{ $pj->razao }}" autofocus>
  					    <span class="help-block hidden" id="input-razao-error"></span>
              </div>
              <div class="form-group has-success" id="input-fantasia-group">
  					    <label for="input-fantasia">Nome fantasia</label>
  					    <input type="text" id="input-fantasia" name="fantasia" class="form-control" placeholder="Ex: Belitalus Prime " maxlength="100" value="{{ $pj->fantasia }}">
  					    <span class="help-block hidden" id="input-fantasia-error"></span>
              </div>
              <div class="form-group has-success" id="input-cep-group">
  					    <label for="input-cep">CEP</label>
  					    <input type="text" id="input-cep" name="cep" class="form-control" placeholder="Ex: 02132576" maxlength="8" value="{{ $pj->cep }}">
  					    <span class="help-block hidden" id="input-cep-error"></span>
              </div>
              <div class="form-group has-success" id="input-logradouro-group">
  					    <label for="input-logradouro">Logradouro</label>
  					    <input type="text" id="input-logradouro" name="logradouro" class="form-control" placeholder="Ex: Av. Alberto Andaló" maxlength="100" value="{{ $pj->logradouro }}">
  					    <span class="help-block hidden" id="input-logradouro-error"></span>
              </div>
              <div class="form-group has-success" id="input-numero-group">
  					    <label for="input-numero">Número</label>
  					    <input type="number" id="input-numero" name="numero" class="form-control" placeholder="Ex: 1032" value="{{ $pj->numero }}">
  					    <span class="help-block hidden" id="input-numero-error"></span>
              </div>
              <div {!! ($pj->complemento == NULL ? "class=\"form-group\"" : "class=\"form-group has-success\"") !!} id="input-complemento-group">
  					    <label for="input-complemento">Complemento</label>
  					    <input type="text" id="input-complemento" name="complemento" class="form-control" placeholder="Ex: Ap 65" maxlength="100" value="{{ $pj->complemento }}">
  					    <span class="help-block hidden" id="input-complemento-error"></span>
              </div>
              <div class="form-group has-success" id="input-bairro-group">
  					    <label for="input-bairro">Bairro</label>
  					    <input type="text" id="input-bairro" name="bairro" class="form-control" placeholder="Ex: Centro" maxlength="100" value="{{ $pj->bairro }}">
  					    <span class="help-block hidden" id="input-bairro-error"></span>
              </div>
              <div class="form-group has-success" id="input-cidade-group">
  					    <label for="input-cidade">Cidade</label>
  					    <input type="text" id="input-cidade" name="cidade" class="form-control" placeholder="Ex: São José do Rio Preto" maxlength="100" value="{{ $pj->cidade }}">
  					    <span class="help-block hidden" id="input-cidade-error"></span>
              </div>
              <div class="form-group has-success" id="input-uf-group">
  						  <label for="input-uf">UF</label>
  						  <select class="form-control" id="input-uf" name="uf">
  							  <option value="VD"></option>
                  @foreach($ufs as $uf)
                  <option value="{{ $uf }}" {!! ($pj->uf == $uf ? "selected=\"selected\"" : "") !!}>{{ $uf }}</option>
                  @endforeach
  						  </select>
  						  <span class="help-block hidden" id="input-uf-error"></span>
  					  </div>
              <br>
              <input type="submit" class="btn btn-primary btn-lg btn-block" value="Atualizar">
            </form>
          </div>
        </div>
      </div>
    </main>
  </div>
  @include('layout.footer')
  <script src="/js/plugins/jquery.mask.min.js"></script>
  <script src="/js/editar-pj.js"></script>
</body>
</html>
