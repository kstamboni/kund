<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Detalhes - Pessoa Física - Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <h1>Detalhes do cadastro</h1>
        <div id="aviso" class="alert alert-success alert-dismissible hidden" style="background-color: white;" role="alert">
        </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <br>
            <table class="table">
              <tr>
                <th>Nome completo</th>
                <td>{{ $pf->nome }} {{ $pf->sobrenome }}</td>
              </tr>
              <tr>
                <th>CPF</th>
                <td>{{ $pf->cpf }}</td>
              </tr>
              <tr>
                <th>Data de nascimento</th>
                <td>{{ \Carbon\Carbon::parse($pf->nascimento)->format('d/m/Y') }}</td>
              </tr>
              <tr>
                <th>Endereço</th>
                <td>{{ $pf->logradouro }}, {{ $pf->numero }}{!! ($pf->complemento != NULL ? ", " : "") !!}{{ $pf->complemento }}</td>
              </tr>
              <tr>
                <th>Bairro</th>
                <td>{{ $pf->bairro }}</td>
              </tr>
              <tr>
                <th>CEP</th>
                <td>{{ $pf->cep }}</td>
              </tr>
              <tr>
                <th>Cidade</th>
                <td>{{ $pf->cidade }} - {{ $pf->uf }}</td>
              </tr>
              <tr><td><td/><tr/>
            </table>
            <div class="btn-group btn-group-justified" role="group" aria-label="Menu de ações: voltar, editar ou excluir o cadastro.">
              <div class="btn-group" role="group">
                <a href="/clientes/listar"><button type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-menu-left"></span>Voltar</button></a>
              </div>
              <div class="btn-group" role="group">
                <a href="/clientes/pf/{{ $pf->id }}/editar"><button type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-pencil"></span>Editar</button></a>
              </div>
              <div class="btn-group" role="group">
                <a><button type="submit" id="btn-excluir" class="btn btn-outline-danger"><span class="glyphicon glyphicon-trash"></span>Excluir</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <form>
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $pf->id }}">
    </form>
  </div>
  @include('layout.footer')
  <script src="/js/plugins/bootbox.min.js"></script>
  <script src="/js/detalhes-pf.js"></script>
</body>
</html>
