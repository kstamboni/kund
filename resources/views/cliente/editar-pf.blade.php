<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Editar - Pessoa Física - Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <h1>Editar cadastro</h1>
        <div class="panel panel-default">
          <div class="panel-body">
            <form id="cadastro-form" onsubmit="return cadastrar_pf()" action="/clientes/pf/{{ $pf->id }}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="tipo" value="PF">
  					  <div class="form-group" id="input-cpf-group">
                <label for="input-cpf">CPF</label>
                <input type="text" id="input-cpf" name="cpf" class="form-control" placeholder="Ex: 211.432.123-10" maxlength="14" value="{{ $pf->cpf }}" disabled>
                <span class="help-block hidden" id="input-cpf-error"></span>
              </div>
              <div class="form-group has-success" id="input-nascimento-group">
              	<label for="input-nascimento">Data de nascimento</label>
                <input type="date" id="input-nascimento" name="nascimento" class="form-control" value="{{ $pf->nascimento }}" autofocus>
                <span class="help-block hidden" id="input-nascimento-error"></span>
              </div>
              <div class="form-group has-success" id="input-nome-group">
              	<label for="input-nome">Nome</label>
                <input type="text" id="input-nome" name="nome" class="form-control" placeholder="Ex: Marcos" maxlength="15" value="{{ $pf->nome }}">
              	<span class="help-block hidden" id="input-nome-error"></span>
              </div>
              <div class="form-group has-success" id="input-sobrenome-group">
              	<label for="input-sobrenome">Sobrenome</label>
              	<input type="text" id="input-sobrenome" name="sobrenome" class="form-control" placeholder="Ex: Silva" maxlength="15" value="{{ $pf->sobrenome }}">
              	<span class="help-block hidden" id="input-sobrenome-error"></span>
              </div>
              <div class="form-group has-success" id="input-cep-group">
  					    <label for="input-cep">CEP</label>
  					    <input type="text" id="input-cep" name="cep" class="form-control" placeholder="Ex: 02132576" maxlength="8" value="{{ $pf->cep }}">
  					    <span class="help-block hidden" id="input-cep-error"></span>
              </div>
              <div class="form-group has-success" id="input-logradouro-group">
  					    <label for="input-logradouro">Logradouro</label>
  					    <input type="text" id="input-logradouro" name="logradouro" class="form-control" placeholder="Ex: Av. Alberto Andaló" maxlength="100" value="{{ $pf->logradouro }}">
  					    <span class="help-block hidden" id="input-logradouro-error"></span>
              </div>
              <div class="form-group has-success" id="input-numero-group">
  					    <label for="input-numero">Número</label>
  					    <input type="number" id="input-numero" name="numero" class="form-control" placeholder="Ex: 1032" value="{{ $pf->numero }}">
  					    <span class="help-block hidden" id="input-numero-error"></span>
              </div>
              <div {!! ($pf->complemento == NULL ? "class=\"form-group\"" : "class=\"form-group has-success\"") !!} id="input-complemento-group">
  					    <label for="input-complemento">Complemento</label>
  					    <input type="text" id="input-complemento" name="complemento" class="form-control" placeholder="Ex: Ap 65" maxlength="100" value="{{ $pf->complemento }}">
  					    <span class="help-block hidden" id="input-complemento-error"></span>
              </div>
              <div class="form-group has-success" id="input-bairro-group">
  					    <label for="input-bairro">Bairro</label>
  					    <input type="text" id="input-bairro" name="bairro" class="form-control" placeholder="Ex: Centro" maxlength="100" value="{{ $pf->bairro }}">
  					    <span class="help-block hidden" id="input-bairro-error"></span>
              </div>
              <div class="form-group has-success" id="input-cidade-group">
  					    <label for="input-cidade">Cidade</label>
  					    <input type="text" id="input-cidade" name="cidade" class="form-control" placeholder="Ex: São José do Rio Preto" maxlength="100" value="{{ $pf->cidade }}">
  					    <span class="help-block hidden" id="input-cidade-error"></span>
              </div>
              <div class="form-group has-success" id="input-uf-group">
  						  <label for="input-uf">UF</label>
  						  <select class="form-control" id="input-uf" name="uf">
  							  <option value="VD"></option>
                  @foreach($ufs as $uf)
                  <option value="{{ $uf }}" {!! ($pf->uf == $uf ? "selected=\"selected\"" : "") !!}>{{ $uf }}</option>
                  @endforeach
  						  </select>
  						  <span class="help-block hidden" id="input-uf-error"></span>
  					  </div>
              <br>
              <input type="submit" class="btn btn-primary btn-lg btn-block" value="Atualizar">
            </form>
          </div>
        </div>
      </div>
    </main>
  </div>
  @include('layout.footer')
  <script src="/js/plugins/jquery.mask.min.js"></script>
  <script src="/js/editar-pf.js"></script>
</body>
</html>
