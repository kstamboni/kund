<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Detalhes - Pessoa Jurídica - Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <h1>Detalhes do cadastro</h1>
        <div id="aviso" class="alert alert-success alert-dismissible hidden" style="background-color: white;" role="alert">
        </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <br>
            <table class="table">
              <tr>
                <th>Nome fantasia</th>
                <td>{{ $pj->fantasia }}</td>
              </tr>
              <tr>
                <th>CNPJ</th>
                <td>{{ $pj->cnpj }}</td>
              </tr>
              <tr>
                <th>Razão social</th>
                <td>{{ $pj->razao }}</td>
              </tr>
              <tr>
                <th>Endereço</th>
                <td>{{ $pj->logradouro }}, {{ $pj->numero }}{!! ($pj->complemento != NULL ? ", " : "") !!}{{ $pj->complemento }}</td>
              </tr>
              <tr>
                <th>Bairro</th>
                <td>{{ $pj->bairro }}</td>
              </tr>
              <tr>
                <th>CEP</th>
                <td>{{ $pj->cep }}</td>
              </tr>
              <tr>
                <th>Cidade</th>
                <td>{{ $pj->cidade }} - {{ $pj->uf }}</td>
              </tr>
              <tr><td><td/><tr/>
            </table>
            <div class="btn-group btn-group-justified" role="group" aria-label="Menu de ações: voltar, editar ou excluir o cadastro.">
              <div class="btn-group" role="group">
                <a href="/clientes/listar"><button type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-menu-left"></span>Voltar</button></a>
              </div>
              <div class="btn-group" role="group">
                <a href="/clientes/pj/{{ $pj->id }}/editar"><button type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-pencil"></span>Editar</button></a>
              </div>
              <div class="btn-group" role="group">
                <a><button type="submit" id="btn-excluir" class="btn btn-outline-danger"><span class="glyphicon glyphicon-trash"></span>Excluir</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <form>
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $pj->id }}">
    </form>
  </div>
  @include('layout.footer')
  <script src="/js/plugins/bootbox.min.js"></script>
  <script src="/js/detalhes-pj.js"></script>  
</body>
</html>
