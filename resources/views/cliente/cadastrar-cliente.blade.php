<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Cadastrar cliente - Kund</title>
  @include('layout.head')
</head>
<body>
  <div class="wrap">
    @include('layout.header')
    <main class="container">
      <div class="col-sm-8 col-sm-offset-2">
        <h1>Cadastrar cliente</h1>
        <div class="panel panel-default">
          <div class="panel-body">
            <label for="input-tipo-pessoa">Que tipo de cliente?</label>
            <div id="input-tipo-pessoa" class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Escolha o cadastro para pessoa física ou jurídica.">
              <div class="btn-group" role="group">
                <button id="button-pf" type="button" class="btn btn-outline-primary">Pessoa Física</button>
              </div>
              <div class="btn-group" role="group">
                <button id="button-pj" type="button" class="btn btn-outline-primary">Pessoa Jurídica</button>
              </div>
            </div>
            <form id="cadastro-form" onsubmit="" action="/clientes" method="post">
              {{ csrf_field() }}
              <div id="campos-dinamicos">
              </div>
            </form>
          </div>
        </div>
      </div>
    </main>
  </div>
  @include('layout.footer')
  <script src="/js/plugins/jquery.mask.min.js"></script>
  <script src="/js/plugins/jquery.cpfcnpj.min.js"></script>
  <script src="/js/cadastrar-cliente.js"></script>
</body>
</html>
