$(document).ready(function() {
  var url = new URL(window.location.href);
  var pam = url.searchParams.get("ec");
  if (pam) {
    var cliente = url.searchParams.get("cliente");
    $('#aviso').append('<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>');
    $('#aviso').append('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span><strong> Sucesso!</strong> O cadastro de ' + cliente + ' foi alterado.');
    $('#aviso').removeClass('hidden');
  }

  $('#btn-excluir').click(function() {
    bootbox.confirm({
      message: "Você irá excluir permanentemente este cadastro. Continuar?",
      buttons: {
        confirm: {
          label: 'Sim',
          className: 'btn-danger'
        },
        cancel: {
          label: 'Não',
          className: 'btn-success'
        }
      },
      callback: function (result) {
        if(result){
          var token = $("input[name=_token]").val();
          var id = $("input[name=id]").val();

          return $.ajax(
           {
             type: 'DELETE',
             url: '/clientes/ajax',
             data:
             {
               '_token': token,
               'tipo': 'PF',
               'id': id,
             },
             dataType: 'json',
             async: true,
             success: function(result){
               window.location.href = result.data.url;
             }
           });
        }
      }
    });
  });
});
