$(document).ready(function() {
      $('#input-nascimento').focusout(function() {
        verificar_nascimento();
      });

      $("#input-nascimento").change(function() {
        verificar_nascimento();
      });

      $('#input-nome').focusout(function() {
        verificar_nome();
      });

      $("#input-nome").change(function() {
        verificar_nome();
      });

      $('#input-sobrenome').focusout(function() {
        verificar_sobrenome();
      });

      $("#input-sobrenome").change(function() {
        verificar_sobrenome();
      });

      $('#input-cep').focusout(function() {
        verificar_cep();
      });

      $("#input-cep").change(function() {
        verificar_cep();
      });

      $('#input-cep').keyup(function () {
        this.value = this.value.replace(/[^0-9]/g,'');
      });

      $('#input-logradouro').focusout(function() {
        verificar_logradouro();
      });

      $("#input-logradouro").change(function() {
        verificar_logradouro();
      });

      $('#input-numero').focusout(function() {
        verificar_numero();
      });

      $("#input-numero").change(function() {
        verificar_numero();
      });

      $('#input-complemento').focusout(function() {
        verificar_complemento();
      });

      $("#input-complemento").change(function() {
        verificar_complemento();
      });

      $('#input-bairro').focusout(function() {
        verificar_bairro();
      });

      $("#input-bairro").change(function() {
        verificar_bairro();
      });

      $('#input-cidade').focusout(function() {
        verificar_cidade();
      });

      $("#input-cidade").change(function() {
        verificar_cidade();
      });

      $('#input-uf').focusout(function() {
        verificar_uf();
      });

      $("#input-uf").change(function() {
        verificar_uf();
      });
  });

function verificar_nascimento() {
  var data_nascimento = $("#input-nascimento").val();

  if (!data_nascimento) {
    $("#input-nascimento-group").addClass("has-error");
    $("#input-nascimento-group").removeClass("has-success");
    $("#input-nascimento-error").removeClass("hidden");
    $("#input-nascimento-error").text("Por favor, informe a data de nascimento.");
    return false;
  }
  else {
    $("#input-nascimento-group").removeClass("has-error");
    $("#input-nascimento-group").addClass("has-success");
    $("#input-nascimento-error").addClass("hidden");
    $("#input-nascimento-error").text("");
  }

  var arr = data_nascimento.split("-");
  var hoje = new Date();
  var idade;

  nsc_ano = parseInt(arr[0]);
  nsc_mes = parseInt(arr[1]) - 1;
  nsc_dia = parseInt(arr[2]);

  hj_dia = hoje.getDate();
  hj_mes = hoje.getMonth();
  hj_ano= hoje.getFullYear();

  if ((hj_mes > nsc_mes) || ( hj_mes === nsc_mes && hj_dia >= nsc_dia))
    idade = hj_ano - nsc_ano;
  else
    idade = hj_ano - nsc_ano - 1;

  if (idade < 19) {
    $("#input-nascimento-group").addClass("has-error");
    $("#input-nascimento-group").removeClass("has-success");
    $("#input-nascimento-error").removeClass("hidden");
    $("#input-nascimento-error").text("É necessário ter pelo menos 19 anos.");
    return false;
  }
  else {
    $("#input-nascimento-group").removeClass("has-error");
    $("#input-nascimento-group").addClass("has-success");
    $("#input-nascimento-error").addClass("hidden");
    $("#input-nascimento-error").text("");
  }

  return true;
}

function verificar_nome() {
  var nome = $("#input-nome").val();

  if(!nome) {
    $("#input-nome-group").addClass("has-error");
    $("#input-nome-group").removeClass("has-success");
    $("#input-nome-error").removeClass("hidden");
    $("#input-nome-error").text("Por favor, digite o nome.");
    return false;
  }
  else {
    $("#input-nome-group").removeClass("has-error");
    $("#input-nome-group").addClass("has-success");
    $("#input-nome-error").addClass("hidden");
    $("#input-nome-error").text("");
  }

  return true;
}

function verificar_sobrenome() {
  var sobrenome = $("#input-sobrenome").val();

  if(!sobrenome) {
    $("#input-sobrenome-group").addClass("has-error");
    $("#input-sobrenome-group").removeClass("has-success");
    $("#input-sobrenome-error").removeClass("hidden");
    $("#input-sobrenome-error").text("Por favor, digite o sobrenome.");
    return false;
  }
  else {
    $("#input-sobrenome-group").removeClass("has-error");
    $("#input-sobrenome-group").addClass("has-success");
    $("#input-sobrenome-error").addClass("hidden");
    $("#input-sobrenome-error").text("");
  }

  return true;
}

function testar_cep() {
  var cep = $("#input-cep").val();

  if(!cep) {
    $("#input-cep-group").addClass("has-error");
    $("#input-cep-group").removeClass("has-warning");
    $("#input-cep-group").removeClass("has-success");
    $("#input-cep-error").removeClass("hidden");
    $("#input-cep-error").text("Por favor, digite o CEP.");
    return false;
  }

  return ($("#input-cep-group").hasClass("has-warning") || $("#input-cep-group").hasClass("has-success"));
}

function verificar_cep() {
  var cep = $("#input-cep").val();

  if(!cep) {
    $("#input-cep-group").addClass("has-error");
    $("#input-cep-group").removeClass("has-warning");
    $("#input-cep-group").removeClass("has-success");
    $("#input-cep-error").removeClass("hidden");
    $("#input-cep-error").text("Por favor, digite o CEP.");
    return false;
  }
  else {
    $("#input-cep-group").removeClass("has-error");
    $("#input-cep-group").removeClass("has-warning");
    $("#input-cep-group").removeClass("has-success");
    $("#input-cep-error").addClass("hidden");
    $("#input-cep-error").text("");

    var validar_cep = /^[0-9]{8}$/;

    if (validar_cep.test(cep)) {
      $("#input-logradouro").val("🔃");
      $("#input-bairro").val("🔃");
      $("#input-cidade").val("🔃");
      $("#input-uf").val("VD");

      $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
          if (!("erro" in dados)) {
              $("#input-logradouro").val(dados.logradouro);
              $("#input-bairro").val(dados.bairro);
              $("#input-cidade").val(dados.localidade);
              $("#input-uf").val(dados.uf);
              $("#input-cep-group").addClass("has-success");
              verificar_logradouro();
              verificar_bairro();
              verificar_cidade();
              verificar_uf();
          }
          else {
            $("#input-cep-group").addClass("has-warning");
            $("#input-cep-error").removeClass("hidden");
            $("#input-cep-error").text("CEP não encontrado. Por favor, digite os dados do endereço.");
            limpar_endereco();
          }
      });
    }
    else {
      $("#input-cep-group").addClass("has-error");
      $("#input-cep-group").removeClass("has-warning");
      $("#input-cep-group").removeClass("has-success");
      $("#input-cep-error").removeClass("hidden");
      $("#input-cep-error").text("CEP inválido. Tente novamente.");
      return false;
    }
  }

  return true;
}

function limpar_endereco() {
  $("#input-logradouro").val("");
  $("#input-logradouro-group").removeClass("has-error");
  $("#input-logradouro-group").removeClass("has-success");
  $("#input-logradouro-error").addClass("hidden");
  $("#input-logradouro-error").text("");
  $("#input-bairro").val("");
  $("#input-bairro-group").removeClass("has-error");
  $("#input-bairro-group").removeClass("has-success");
  $("#input-bairro-error").addClass("hidden");
  $("#input-bairro-error").text("");
  $("#input-cidade").val("");
  $("#input-cidade-group").removeClass("has-error");
  $("#input-cidade-group").removeClass("has-success");
  $("#input-cidade-error").addClass("hidden");
  $("#input-cidade-error").text("");
  $("#input-uf").val("VD");
  $("#input-uf-group").removeClass("has-error");
  $("#input-uf-group").removeClass("has-success");
  $("#input-uf-error").addClass("hidden");
  $("#input-uf-error").text("");
}

function verificar_logradouro() {
  var logradouro = $("#input-logradouro").val();

  if (!logradouro) {
    $("#input-logradouro-group").addClass("has-error");
    $("#input-logradouro-group").removeClass("has-success");
    $("#input-logradouro-error").removeClass("hidden");
    if ($("#input-cep-group").hasClass("has-warning"))
      $("#input-logradouro-error").text("Por favor, digite o logradouro.");
    else
      $("#input-logradouro-error").text("Por favor, informe o CEP para preenchimento automático do logradouro.");
    return false;
  }
  else {
    $("#input-logradouro-group").removeClass("has-error");
    $("#input-logradouro-group").addClass("has-success");
    $("#input-logradouro-error").addClass("hidden");
    $("#input-logradouro-error").text("");
  }

  return true;
}

function verificar_numero() {
  var numero = $("#input-numero").val();

  if(!numero) {
    $("#input-numero-group").addClass("has-error");
    $("#input-numero-group").removeClass("has-success");
    $("#input-numero-error").removeClass("hidden");
    $("#input-numero-error").text("Por favor, digite o número.");
    return false;
  }
  else {
    $("#input-numero-group").removeClass("has-error");
    $("#input-numero-group").addClass("has-success");
    $("#input-numero-error").addClass("hidden");
    $("#input-numero-error").text("");
  }

  return true;
}

function verificar_complemento() {
  var complemento = $("#input-complemento").val();

  if(!complemento)
    $("#input-complemento-group").removeClass("has-success");
  else
    $("#input-complemento-group").addClass("has-success");

  return true;
}

function verificar_bairro() {
  var bairro = $("#input-bairro").val();

  if(!bairro) {
    $("#input-bairro-group").addClass("has-error");
    $("#input-bairro-group").removeClass("has-success");
    $("#input-bairro-error").removeClass("hidden");
    if ($("#input-cep-group").hasClass("has-warning"))
      $("#input-bairro-error").text("Por favor, digite o bairro.");
    else
      $("#input-bairro-error").text("Por favor, informe o CEP para preenchimento automático do bairro.");
    return false;
  }
  else {
    $("#input-bairro-group").removeClass("has-error");
    $("#input-bairro-group").addClass("has-success");
    $("#input-bairro-error").addClass("hidden");
    $("#input-bairro-error").text("");
  }

  return true;
}

function verificar_cidade() {
  var cidade = $("#input-cidade").val();

  if(!cidade) {
    $("#input-cidade-group").addClass("has-error");
    $("#input-cidade-group").removeClass("has-success");
    $("#input-cidade-error").removeClass("hidden");
    if ($("#input-cep-group").hasClass("has-warning"))
      $("#input-cidade-error").text("Por favor, digite a cidade.");
    else
      $("#input-cidade-error").text("Por favor, informe o CEP para preenchimento automático da cidade.");
    return false;
  }
  else {
    $("#input-cidade-group").removeClass("has-error");
    $("#input-cidade-group").addClass("has-success");
    $("#input-cidade-error").addClass("hidden");
    $("#input-cidade-error").text("");
  }

  return true;
}

function verificar_uf() {
  var uf = $("#input-uf").val();

  if(uf === "VD") {
    $("#input-uf-group").addClass("has-error");
    $("#input-uf-group").removeClass("has-success");
    $("#input-uf-error").removeClass("hidden");
    $("#input-uf-error").text("Por favor, escolha a unidade federativa.");
    return false;
  }
  else {
    $("#input-uf-group").removeClass("has-error");
    $("#input-uf-group").addClass("has-success");
    $("#input-uf-error").addClass("hidden");
    $("#input-uf-error").text("");
  }

  return true;
}

function cadastrar_pf() {
  var nascimento_ok = verificar_nascimento();
  var nome_ok = verificar_nome();
  var sobrenome_ok = verificar_sobrenome();
  var cep_ok = testar_cep();
  var logradouro_ok = verificar_logradouro();
  var numero_ok = verificar_numero();
  var bairro_ok = verificar_bairro();
  var cidade_ok = verificar_cidade();
  var uf_ok = verificar_uf();

  return (nascimento_ok && nome_ok &&
          sobrenome_ok && cep_ok && logradouro_ok &&
          numero_ok && bairro_ok && cidade_ok && uf_ok);
}
