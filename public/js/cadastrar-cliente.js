$(document).ready(function() {
    function criar_formulario_pf() {
      var formulario_pf = '\
      <hr>\
      <input type="hidden" name="tipo" value="PF">\
      <div class="form-group" id="input-cpf-group">\
        <label for="input-cpf">CPF</label>\
        <input type="text" id="input-cpf" name="cpf" class="form-control" placeholder="Ex: 211.432.123-10" maxlength="14" autofocus>\
        <span class="help-block hidden" id="input-cpf-error"></span>\
      </div>\
      <div class="form-group" id="input-nascimento-group">\
        <label for="input-nascimento">Data de nascimento</label>\
        <input type="date" id="input-nascimento" name="nascimento" class="form-control">\
        <span class="help-block hidden" id="input-nascimento-error"></span>\
      </div>\
      <div class="form-group" id="input-nome-group">\
        <label for="input-nome">Nome</label>\
        <input type="text" id="input-nome" name="nome" class="form-control" placeholder="Ex: Marcos" maxlength="15">\
        <span class="help-block hidden" id="input-nome-error"></span>\
      </div>\
      <div class="form-group" id="input-sobrenome-group">\
        <label for="input-sobrenome">Sobrenome</label>\
        <input type="text" id="input-sobrenome" name="sobrenome" class="form-control" placeholder="Ex: Silva" maxlength="15">\
        <span class="help-block hidden" id="input-sobrenome-error"></span>\
      </div>\
      <div class="form-group" id="input-cep-group">\
        <label for="input-cep">CEP</label>\
        <input type="text" id="input-cep" name="cep" class="form-control" placeholder="Ex: 02132576" maxlength="8">\
        <span class="help-block hidden" id="input-cep-error"></span>\
      </div>\
      <div class="form-group" id="input-logradouro-group">\
        <label for="input-logradouro">Logradouro</label>\
        <input type="text" id="input-logradouro" name="logradouro" class="form-control" placeholder="Ex: Av. Alberto Andaló" maxlength="100">\
        <span class="help-block hidden" id="input-logradouro-error"></span>\
      </div>\
      <div class="form-group" id="input-numero-group">\
        <label for="input-numero">Número</label>\
        <input type="number" id="input-numero" name="numero" class="form-control" placeholder="Ex: 1032">\
        <span class="help-block hidden" id="input-numero-error"></span>\
      </div>\
      <div class="form-group" id="input-complemento-group">\
        <label for="input-complemento">Complemento</label>\
        <input type="text" id="input-complemento" name="complemento" class="form-control" placeholder="Ex: Ap 65" maxlength="100">\
        <span class="help-block hidden" id="input-complemento-error"></span>\
      </div>\
      <div class="form-group" id="input-bairro-group">\
        <label for="input-bairro">Bairro</label>\
        <input type="text" id="input-bairro" name="bairro" class="form-control" placeholder="Ex: Centro" maxlength="100">\
        <span class="help-block hidden" id="input-bairro-error"></span>\
      </div>\
      <div class="form-group" id="input-cidade-group">\
        <label for="input-cidade">Cidade</label>\
        <input type="text" id="input-cidade" name="cidade" class="form-control" placeholder="Ex: São José do Rio Preto" maxlength="100">\
        <span class="help-block hidden" id="input-cidade-error"></span>\
      </div>\
      <div class="form-group" id="input-uf-group">\
        <label for="input-uf">UF</label>\
        <select class="form-control" id="input-uf" name="uf">\
          <option value="VD"></option>\
          <option value="AC">AC</option>\
          <option value="AL">AL</option>\
          <option value="AP">AP</option>\
          <option value="AM">AM</option>\
          <option value="BA">BA</option>\
          <option value="CE">CE</option>\
          <option value="DF">DF</option>\
          <option value="ES">ES</option>\
          <option value="GO">GO</option>\
          <option value="MA">MA</option>\
          <option value="MT">MT</option>\
          <option value="MS">MS</option>\
          <option value="MG">MG</option>\
          <option value="PA">PA</option>\
          <option value="PB">PB</option>\
          <option value="PR">PR</option>\
          <option value="PE">PE</option>\
          <option value="PI">PI</option>\
          <option value="RJ">RJ</option>\
          <option value="RN">RN</option>\
          <option value="RS">RS</option>\
          <option value="RO">RO</option>\
          <option value="RR">RR</option>\
          <option value="SC">SC</option>\
          <option value="SP">SP</option>\
          <option value="SE">SE</option>\
          <option value="TO">TO</option>\
        </select>\
        <span class="help-block hidden" id="input-uf-error"></span>\
      </div>\
      <br>\
      <input type="submit" class="btn btn-primary btn-lg btn-block" value="Cadastrar">';

      $("#campos-dinamicos").off();
      $("#campos-dinamicos").empty();
      $("#campos-dinamicos").append(formulario_pf);
      $("#cadastro-form").attr("onsubmit", "return cadastrar_pf()");
      $("#button-pj").removeClass("active");
      $("#button-pf").addClass("active");

      $("#input-cpf").cpfcnpj({
        mask: true,
        validate: 'cpf',
        event: 'change',
        handler: '#input-cpf',
        ifValid: function() {
          $("#input-cpf-group").removeClass("has-error");
          $("#input-cpf-group").addClass("has-success");
          $("#input-cpf-error").addClass("hidden");
          $("#input-cpf-error").text("");

          var result = autenticar("PF");

          if (!(result.responseJSON.data.ok)){
            $("#input-cpf-group").addClass("has-error");
            $("#input-cpf-group").removeClass("has-success");
            $("#input-cpf-error").removeClass("hidden");
            $("#input-cpf-error").text("CPF já registrado. Tente novamente.");
          }
        },
        ifInvalid: function() {
          $("#input-cpf-group").addClass("has-error");
          $("#input-cpf-group").removeClass("has-success");
          $("#input-cpf-error").removeClass("hidden");
          $("#input-cpf-error").text("CPF inválido. Tente novamente.");
        }
      });

      $('#input-cpf').focusout(function() {
        verificar_cpf();
      });

      $('#input-nascimento').focusout(function() {
        verificar_nascimento();
      });

      $("#input-nascimento").change(function() {
        verificar_nascimento();
      });

      $('#input-nome').focusout(function() {
        verificar_nome();
      });

      $("#input-nome").change(function() {
        verificar_nome();
      });

      $('#input-sobrenome').focusout(function() {
        verificar_sobrenome();
      });

      $("#input-sobrenome").change(function() {
        verificar_sobrenome();
      });

      $('#input-cep').focusout(function() {
        verificar_cep();
      });

      $("#input-cep").change(function() {
        verificar_cep();
      });

      $('#input-cep').keyup(function () {
        this.value = this.value.replace(/[^0-9]/g,'');
      });

      $('#input-logradouro').focusout(function() {
        verificar_logradouro();
      });

      $("#input-logradouro").change(function() {
        verificar_logradouro();
      });

      $('#input-numero').focusout(function() {
        verificar_numero();
      });

      $("#input-numero").change(function() {
        verificar_numero();
      });

      $('#input-complemento').focusout(function() {
        verificar_complemento();
      });

      $("#input-complemento").change(function() {
        verificar_complemento();
      });

      $('#input-bairro').focusout(function() {
        verificar_bairro();
      });

      $("#input-bairro").change(function() {
        verificar_bairro();
      });

      $('#input-cidade').focusout(function() {
        verificar_cidade();
      });

      $("#input-cidade").change(function() {
        verificar_cidade();
      });

      $('#input-uf').focusout(function() {
        verificar_uf();
      });

      $("#input-uf").change(function() {
        verificar_uf();
      });
    }

    $("#button-pf").click(function() {
      if (!($("#button-pf").hasClass("active")))
        criar_formulario_pf();
    });

    function criar_formulario_pj() {
      var formulario_pj = '\
      <hr>\
      <input type="hidden" name="tipo" value="PJ">\
      <div class="form-group" id="input-cnpj-group">\
        <label for="input-cnpj">CNPJ</label>\
        <input type="text" id="input-cnpj" name="cnpj" class="form-control" placeholder="Ex: 71.526.721/0001-32" maxlength="18" autofocus>\
        <span class="help-block hidden" id="input-cnpj-error"></span>\
      </div>\
      <div class="form-group" id="input-razao-group">\
        <label for="input-razao">Razão social</label>\
        <input type="text" id="input-razao" name="razao" class="form-control" placeholder="Ex: Belitalus Prime Transportes do Brasil S/A" maxlength="100">\
        <span class="help-block hidden" id="input-razao-error"></span>\
      </div>\
      <div class="form-group" id="input-fantasia-group">\
        <label for="input-fantasia">Nome fantasia</label>\
        <input type="text" id="input-fantasia" name="fantasia" class="form-control" placeholder="Ex: Belitalus Prime " maxlength="100">\
        <span class="help-block hidden" id="input-fantasia-error"></span>\
      </div>\
      <div class="form-group" id="input-cep-group">\
        <label for="input-cep">CEP</label>\
        <input type="text" id="input-cep" name="cep" class="form-control" placeholder="Ex: 02132576" maxlength="8">\
        <span class="help-block hidden" id="input-cep-error"></span>\
      </div>\
      <div class="form-group" id="input-logradouro-group">\
        <label for="input-logradouro">Logradouro</label>\
        <input type="text" id="input-logradouro" name="logradouro" class="form-control" placeholder="Ex: Av. Alberto Andaló" maxlength="100">\
        <span class="help-block hidden" id="input-logradouro-error"></span>\
      </div>\
      <div class="form-group" id="input-numero-group">\
        <label for="input-numero">Número</label>\
        <input type="number" id="input-numero" name="numero" class="form-control" placeholder="Ex: 1032">\
        <span class="help-block hidden" id="input-numero-error"></span>\
      </div>\
      <div class="form-group" id="input-complemento-group">\
        <label for="input-complemento">Complemento</label>\
        <input type="text" id="input-complemento" name="complemento" class="form-control" placeholder="Ex: Ap 65" maxlength="100">\
        <span class="help-block hidden" id="input-complemento-error"></span>\
      </div>\
      <div class="form-group" id="input-bairro-group">\
        <label for="input-bairro">Bairro</label>\
        <input type="text" id="input-bairro" name="bairro" class="form-control" placeholder="Ex: Centro" maxlength="100">\
        <span class="help-block hidden" id="input-bairro-error"></span>\
      </div>\
      <div class="form-group" id="input-cidade-group">\
        <label for="input-cidade">Cidade</label>\
        <input type="text" id="input-cidade" name="cidade" class="form-control" placeholder="Ex: São José do Rio Preto" maxlength="100">\
        <span class="help-block hidden" id="input-cidade-error"></span>\
      </div>\
      <div class="form-group" id="input-uf-group">\
        <label for="input-uf">UF</label>\
        <select class="form-control" id="input-uf" name="uf">\
          <option value="VD"></option>\
          <option value="AC">AC</option>\
          <option value="AL">AL</option>\
          <option value="AP">AP</option>\
          <option value="AM">AM</option>\
          <option value="BA">BA</option>\
          <option value="CE">CE</option>\
          <option value="DF">DF</option>\
          <option value="ES">ES</option>\
          <option value="GO">GO</option>\
          <option value="MA">MA</option>\
          <option value="MT">MT</option>\
          <option value="MS">MS</option>\
          <option value="MG">MG</option>\
          <option value="PA">PA</option>\
          <option value="PB">PB</option>\
          <option value="PR">PR</option>\
          <option value="PE">PE</option>\
          <option value="PI">PI</option>\
          <option value="RJ">RJ</option>\
          <option value="RN">RN</option>\
          <option value="RS">RS</option>\
          <option value="RO">RO</option>\
          <option value="RR">RR</option>\
          <option value="SC">SC</option>\
          <option value="SP">SP</option>\
          <option value="SE">SE</option>\
          <option value="TO">TO</option>\
        </select>\
        <span class="help-block hidden" id="input-uf-error"></span>\
      </div>\
      <br>\
      <input type="submit" class="btn btn-primary btn-lg btn-block" value="Cadastrar">';

      $("#campos-dinamicos").off();
      $("#campos-dinamicos").empty();
      $("#campos-dinamicos").append(formulario_pj);
      $("#cadastro-form").attr("onsubmit", "return cadastrar_pj()");
      $("#button-pf").removeClass("active");
      $("#button-pj").addClass("active");

      $("#input-cnpj").cpfcnpj({
        mask: true,
        validate: 'cnpj',
        event: 'change',
        handler: '#input-cnpj',
        ifValid: function() {
          $("#input-cnpj-group").removeClass("has-error");
          $("#input-cnpj-group").addClass("has-success");
          $("#input-cnpj-error").addClass("hidden");
          $("#input-cnpj-error").text("");

          var result = autenticar("PJ");

          if (!(result.responseJSON.data.ok)){
            $("#input-cnpj-group").addClass("has-error");
            $("#input-cnpj-group").removeClass("has-success");
            $("#input-cnpj-error").removeClass("hidden");
            $("#input-cnpj-error").text("CNPJ já registrado. Tente novamente.");
          }
        },
        ifInvalid: function() {
          $("#input-cnpj-group").addClass("has-error");
          $("#input-cnpj-group").removeClass("has-success");
          $("#input-cnpj-error").removeClass("hidden");
          $("#input-cnpj-error").text("CNPJ inválido. Tente novamente.");
        }
      });

      $('#input-cnpj').focusout(function() {
        verificar_cnpj();
      });

      $('#input-razao').focusout(function() {
        verificar_razao();
      });

      $("#input-razao").change(function() {
        verificar_razao();
      });

      $('#input-fantasia').focusout(function() {
        verificar_fantasia();
      });

      $("#input-fantasia").change(function() {
        verificar_fantasia();
      });

      $('#input-nascimento').focusout(function() {
        verificar_nascimento();
      });

      $("#input-nascimento").change(function() {
        verificar_nascimento();
      });

      $('#input-nome').focusout(function() {
        verificar_nome();
      });

      $("#input-nome").change(function() {
        verificar_nome();
      });

      $('#input-sobrenome').focusout(function() {
        verificar_sobrenome();
      });

      $("#input-sobrenome").change(function() {
        verificar_sobrenome();
      });

      $('#input-cep').focusout(function() {
        verificar_cep();
      });

      $("#input-cep").change(function() {
        verificar_cep();
      });

      $('#input-cep').keyup(function () {
        this.value = this.value.replace(/[^0-9]/g,'');
      });

      $('#input-logradouro').focusout(function() {
        verificar_logradouro();
      });

      $("#input-logradouro").change(function() {
        verificar_logradouro();
      });

      $('#input-numero').focusout(function() {
        verificar_numero();
      });

      $("#input-numero").change(function() {
        verificar_numero();
      });

      $('#input-complemento').focusout(function() {
        verificar_complemento();
      });

      $("#input-complemento").change(function() {
        verificar_complemento();
      });

      $('#input-bairro').focusout(function() {
        verificar_bairro();
      });

      $("#input-bairro").change(function() {
        verificar_bairro();
      });

      $('#input-cidade').focusout(function() {
        verificar_cidade();
      });

      $("#input-cidade").change(function() {
        verificar_cidade();
      });

      $('#input-uf').focusout(function() {
        verificar_uf();
      });

      $("#input-uf").change(function() {
        verificar_uf();
      });
    }

    $("#button-pj").click(function() {
      if (!($("#button-pj").hasClass("active")))
        criar_formulario_pj();
    });
  });

function verificar_cnpj() {
  var cnpj = $("#input-cnpj").val();

  if(!cnpj) {
    $("#input-cnpj-group").addClass("has-error");
    $("#input-cnpj-group").removeClass("has-success");
    $("#input-cnpj-error").removeClass("hidden");
    $("#input-cnpj-error").text("Por favor, digite o CNPJ.");
    return false;
  }
  else if($("#input-cnpj-group").hasClass("has-error")){
    return false;
  }

  return true;
}

function verificar_razao() {
  var razao = $("#input-razao").val();

  if(!razao) {
    $("#input-razao-group").addClass("has-error");
    $("#input-razao-group").removeClass("has-success");
    $("#input-razao-error").removeClass("hidden");
    $("#input-razao-error").text("Por favor, digite a razão social.");
    return false;
  }
  else {
    $("#input-razao-group").removeClass("has-error");
    $("#input-razao-group").addClass("has-success");
    $("#input-razao-error").addClass("hidden");
    $("#input-razao-error").text("");
  }

  return true;
}

function verificar_fantasia() {
  var fantasia = $("#input-fantasia").val();

  if(!fantasia) {
    $("#input-fantasia-group").addClass("has-error");
    $("#input-fantasia-group").removeClass("has-success");
    $("#input-fantasia-error").removeClass("hidden");
    $("#input-fantasia-error").text("Por favor, digite o nome fantasia.");
    return false;
  }
  else {
    $("#input-fantasia-group").removeClass("has-error");
    $("#input-fantasia-group").addClass("has-success");
    $("#input-fantasia-error").addClass("hidden");
    $("#input-fantasia-error").text("");
  }

  return true;
}

function verificar_cpf() {
  var cpf = $("#input-cpf").val();

  if(!cpf) {
    $("#input-cpf-group").addClass("has-error");
    $("#input-cpf-group").removeClass("has-success");
    $("#input-cpf-error").removeClass("hidden");
    $("#input-cpf-error").text("Por favor, digite o CPF.");
    return false;
  }
  else if($("#input-cpf-group").hasClass("has-error")){
    return false;
  }

  return true;
}

function verificar_nascimento() {
  var data_nascimento = $("#input-nascimento").val();

  if (!data_nascimento) {
    $("#input-nascimento-group").addClass("has-error");
    $("#input-nascimento-group").removeClass("has-success");
    $("#input-nascimento-error").removeClass("hidden");
    $("#input-nascimento-error").text("Por favor, informe a data de nascimento.");
    return false;
  }
  else {
    $("#input-nascimento-group").removeClass("has-error");
    $("#input-nascimento-group").addClass("has-success");
    $("#input-nascimento-error").addClass("hidden");
    $("#input-nascimento-error").text("");
  }

  var arr = data_nascimento.split("-");
  var hoje = new Date();
  var idade;

  nsc_ano = parseInt(arr[0]);
  nsc_mes = parseInt(arr[1]) - 1;
  nsc_dia = parseInt(arr[2]);

  hj_dia = hoje.getDate();
  hj_mes = hoje.getMonth();
  hj_ano= hoje.getFullYear();

  if ((hj_mes > nsc_mes) || ( hj_mes === nsc_mes && hj_dia >= nsc_dia))
    idade = hj_ano - nsc_ano;
  else
    idade = hj_ano - nsc_ano - 1;

  if (idade < 19) {
    $("#input-nascimento-group").addClass("has-error");
    $("#input-nascimento-group").removeClass("has-success");
    $("#input-nascimento-error").removeClass("hidden");
    $("#input-nascimento-error").text("É necessário ter pelo menos 19 anos.");
    return false;
  }
  else {
    $("#input-nascimento-group").removeClass("has-error");
    $("#input-nascimento-group").addClass("has-success");
    $("#input-nascimento-error").addClass("hidden");
    $("#input-nascimento-error").text("");
  }

  return true;
}

function verificar_nome() {
  var nome = $("#input-nome").val();

  if(!nome) {
    $("#input-nome-group").addClass("has-error");
    $("#input-nome-group").removeClass("has-success");
    $("#input-nome-error").removeClass("hidden");
    $("#input-nome-error").text("Por favor, digite o nome.");
    return false;
  }
  else {
    $("#input-nome-group").removeClass("has-error");
    $("#input-nome-group").addClass("has-success");
    $("#input-nome-error").addClass("hidden");
    $("#input-nome-error").text("");
  }

  return true;
}

function verificar_sobrenome() {
  var sobrenome = $("#input-sobrenome").val();

  if(!sobrenome) {
    $("#input-sobrenome-group").addClass("has-error");
    $("#input-sobrenome-group").removeClass("has-success");
    $("#input-sobrenome-error").removeClass("hidden");
    $("#input-sobrenome-error").text("Por favor, digite o sobrenome.");
    return false;
  }
  else {
    $("#input-sobrenome-group").removeClass("has-error");
    $("#input-sobrenome-group").addClass("has-success");
    $("#input-sobrenome-error").addClass("hidden");
    $("#input-sobrenome-error").text("");
  }

  return true;
}

function testar_cep() {
  var cep = $("#input-cep").val();

  if(!cep) {
    $("#input-cep-group").addClass("has-error");
    $("#input-cep-group").removeClass("has-warning");
    $("#input-cep-group").removeClass("has-success");
    $("#input-cep-error").removeClass("hidden");
    $("#input-cep-error").text("Por favor, digite o CEP.");
    return false;
  }

  return ($("#input-cep-group").hasClass("has-warning") || $("#input-cep-group").hasClass("has-success"));
}

function verificar_cep() {
  var cep = $("#input-cep").val();

  if(!cep) {
    $("#input-cep-group").addClass("has-error");
    $("#input-cep-group").removeClass("has-warning");
    $("#input-cep-group").removeClass("has-success");
    $("#input-cep-error").removeClass("hidden");
    $("#input-cep-error").text("Por favor, digite o CEP.");
    return false;
  }
  else {
    $("#input-cep-group").removeClass("has-error");
    $("#input-cep-group").removeClass("has-warning");
    $("#input-cep-group").removeClass("has-success");
    $("#input-cep-error").addClass("hidden");
    $("#input-cep-error").text("");

    var validar_cep = /^[0-9]{8}$/;

    if (validar_cep.test(cep)) {
      $("#input-logradouro").val("🔃");
      $("#input-bairro").val("🔃");
      $("#input-cidade").val("🔃");
      $("#input-uf").val("VD");

      $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
          if (!("erro" in dados)) {
              $("#input-logradouro").val(dados.logradouro);
              $("#input-bairro").val(dados.bairro);
              $("#input-cidade").val(dados.localidade);
              $("#input-uf").val(dados.uf);
              $("#input-cep-group").addClass("has-success");
              verificar_logradouro();
              verificar_bairro();
              verificar_cidade();
              verificar_uf();
          }
          else {
            $("#input-cep-group").addClass("has-warning");
            $("#input-cep-error").removeClass("hidden");
            $("#input-cep-error").text("CEP não encontrado. Por favor, digite os dados do endereço.");
            limpar_endereco();
          }
      });
    }
    else {
      $("#input-cep-group").addClass("has-error");
      $("#input-cep-group").removeClass("has-warning");
      $("#input-cep-group").removeClass("has-success");
      $("#input-cep-error").removeClass("hidden");
      $("#input-cep-error").text("CEP inválido. Tente novamente.");
      return false;
    }
  }

  return true;
}

function limpar_endereco() {
  $("#input-logradouro").val("");
  $("#input-logradouro-group").removeClass("has-error");
  $("#input-logradouro-group").removeClass("has-success");
  $("#input-logradouro-error").addClass("hidden");
  $("#input-logradouro-error").text("");
  $("#input-bairro").val("");
  $("#input-bairro-group").removeClass("has-error");
  $("#input-bairro-group").removeClass("has-success");
  $("#input-bairro-error").addClass("hidden");
  $("#input-bairro-error").text("");
  $("#input-cidade").val("");
  $("#input-cidade-group").removeClass("has-error");
  $("#input-cidade-group").removeClass("has-success");
  $("#input-cidade-error").addClass("hidden");
  $("#input-cidade-error").text("");
  $("#input-uf").val("VD");
  $("#input-uf-group").removeClass("has-error");
  $("#input-uf-group").removeClass("has-success");
  $("#input-uf-error").addClass("hidden");
  $("#input-uf-error").text("");
}

function verificar_logradouro() {
  var logradouro = $("#input-logradouro").val();

  if (!logradouro) {
    $("#input-logradouro-group").addClass("has-error");
    $("#input-logradouro-group").removeClass("has-success");
    $("#input-logradouro-error").removeClass("hidden");
    if ($("#input-cep-group").hasClass("has-warning"))
      $("#input-logradouro-error").text("Por favor, digite o logradouro.");
    else
      $("#input-logradouro-error").text("Por favor, informe o CEP para preenchimento automático do logradouro.");
    return false;
  }
  else {
    $("#input-logradouro-group").removeClass("has-error");
    $("#input-logradouro-group").addClass("has-success");
    $("#input-logradouro-error").addClass("hidden");
    $("#input-logradouro-error").text("");
  }

  return true;
}

function verificar_numero() {
  var numero = $("#input-numero").val();

  if(!numero) {
    $("#input-numero-group").addClass("has-error");
    $("#input-numero-group").removeClass("has-success");
    $("#input-numero-error").removeClass("hidden");
    $("#input-numero-error").text("Por favor, digite o número.");
    return false;
  }
  else {
    $("#input-numero-group").removeClass("has-error");
    $("#input-numero-group").addClass("has-success");
    $("#input-numero-error").addClass("hidden");
    $("#input-numero-error").text("");
  }

  return true;
}

function verificar_complemento() {
  var complemento = $("#input-complemento").val();

  if(!complemento)
    $("#input-complemento-group").removeClass("has-success");
  else
    $("#input-complemento-group").addClass("has-success");

  return true;
}

function verificar_bairro() {
  var bairro = $("#input-bairro").val();

  if(!bairro) {
    $("#input-bairro-group").addClass("has-error");
    $("#input-bairro-group").removeClass("has-success");
    $("#input-bairro-error").removeClass("hidden");
    if ($("#input-cep-group").hasClass("has-warning"))
      $("#input-bairro-error").text("Por favor, digite o bairro.");
    else
      $("#input-bairro-error").text("Por favor, informe o CEP para preenchimento automático do bairro.");
    return false;
  }
  else {
    $("#input-bairro-group").removeClass("has-error");
    $("#input-bairro-group").addClass("has-success");
    $("#input-bairro-error").addClass("hidden");
    $("#input-bairro-error").text("");
  }

  return true;
}

function verificar_cidade() {
  var cidade = $("#input-cidade").val();

  if(!cidade) {
    $("#input-cidade-group").addClass("has-error");
    $("#input-cidade-group").removeClass("has-success");
    $("#input-cidade-error").removeClass("hidden");
    if ($("#input-cep-group").hasClass("has-warning"))
      $("#input-cidade-error").text("Por favor, digite a cidade.");
    else
      $("#input-cidade-error").text("Por favor, informe o CEP para preenchimento automático da cidade.");
    return false;
  }
  else {
    $("#input-cidade-group").removeClass("has-error");
    $("#input-cidade-group").addClass("has-success");
    $("#input-cidade-error").addClass("hidden");
    $("#input-cidade-error").text("");
  }

  return true;
}

function verificar_uf() {
  var uf = $("#input-uf").val();

  if(uf === "VD") {
    $("#input-uf-group").addClass("has-error");
    $("#input-uf-group").removeClass("has-success");
    $("#input-uf-error").removeClass("hidden");
    $("#input-uf-error").text("Por favor, escolha a unidade federativa.");
    return false;
  }
  else {
    $("#input-uf-group").removeClass("has-error");
    $("#input-uf-group").addClass("has-success");
    $("#input-uf-error").addClass("hidden");
    $("#input-uf-error").text("");
  }

  return true;
}

function cadastrar_pf() {
  var cpf_ok = verificar_cpf();
  var nascimento_ok = verificar_nascimento();
  var nome_ok = verificar_nome();
  var sobrenome_ok = verificar_sobrenome();
  var cep_ok = testar_cep();
  var logradouro_ok = verificar_logradouro();
  var numero_ok = verificar_numero();
  var bairro_ok = verificar_bairro();
  var cidade_ok = verificar_cidade();
  var uf_ok = verificar_uf();

  return (cpf_ok && nascimento_ok && nome_ok &&
          sobrenome_ok && cep_ok && logradouro_ok &&
          numero_ok && bairro_ok && cidade_ok && uf_ok);
}

function cadastrar_pj() {
  var cnpj_ok = verificar_cnpj();
  var razao_ok = verificar_razao();
  var fantasia_ok = verificar_fantasia();
  var cep_ok = testar_cep();
  var logradouro_ok = verificar_logradouro();
  var numero_ok = verificar_numero();
  var bairro_ok = verificar_bairro();
  var cidade_ok = verificar_cidade();
  var uf_ok = verificar_uf();

  return (cnpj_ok && razao_ok && fantasia_ok && cep_ok && logradouro_ok &&
          numero_ok && bairro_ok && cidade_ok && uf_ok);
}

function autenticar(tipo) {

  var token = $("input[name=_token]").val();
  var cliente;

  if(tipo == 'PF') {
    cliente = $("input[name=cpf]").val();
  }
  else if(tipo == 'PJ') {
    cliente = $("input[name=cnpj]").val();
  }
  else {
    return;
  }

  return $.ajax(
   {
     type: 'POST',
     url: '/clientes/ajax/validar',
     data:
     {
       '_token': token,
       'tipo': tipo,
       'cliente': cliente,
     },
     dataType: 'json',
     async: false,
   });
}
