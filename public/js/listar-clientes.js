$(document).ready(function() {
  var url = new URL(window.location.href);
  var pam = url.searchParams.get("dc");
  if (pam) {
    var cliente = url.searchParams.get("cliente");
    $('#aviso').append('<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>');
    $('#aviso').append('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span><strong> Sucesso!</strong> O cadastro de ' + cliente + ' foi excluído.');
    $('#aviso').removeClass('hidden');
  }

  pam = url.searchParams.get("nc");
  if (pam) {
    var cliente = url.searchParams.get("cliente");
    $('#aviso').append('<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>');
    $('#aviso').append('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span><strong> Sucesso!</strong> O cadastro de ' + cliente + ' foi concluído.');
    $('#aviso').removeClass('hidden');
  }
});
