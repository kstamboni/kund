<?php

namespace kund;

use Illuminate\Database\Eloquent\Model;

class Pf extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'pfs';
}
