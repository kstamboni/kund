<?php

namespace kund\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use kund\Pf;
use kund\Pj;

class ClienteController extends Controller
{
  /* Lista em uma página HTML todos os clientes (pessoas
     físicas e jurídicas) registrados no banco de dados. */
  public function lista_todos(){
    $pfs = Pf::all();
    $pjs = Pj::all();

    return view('cliente.listar-clientes')->with('pfs', $pfs)->with('pjs', $pjs);
  }

  /* Responde com uma página HTML com os dados registrados
     de uma pessoa física correspondente ao id informado. */
  public function apresenta_pf($id){
    $pf = Pf::find($id);

    if (empty($pf)) {
      abort(404);
    }

    return view('cliente.detalhes-pf')->with('pf', $pf);
  }

  /* Responde com uma página HTML com os dados registrados
     de uma pessoa jurídica correspondente ao id informado. */
  public function apresenta_pj($id){
    $pj = Pj::find($id);

    if (empty($pj)) {
      abort(404);
    }

    return view('cliente.detalhes-pj')->with('pj', $pj);
  }

  /* Responde com um formulário HTML para alteração dos dados
     de uma pessoa física específica (id). */
  public function apresenta_edicao_pf($id){
    $pf = Pf::find($id);
    $ufs = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA',
            'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN',
            'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

    if (empty($pf)) {
      abort(404);
    }

    return view('cliente.editar-pf')->with('pf', $pf)->with('ufs', $ufs);
  }

  /* Responde com um formulário HTML para alteração dos dados
     de uma pessoa jurídica específica (id). */
  public function apresenta_edicao_pj($id){
    $pj = Pj::find($id);
    $ufs = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA',
            'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN',
            'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

    if (empty($pj)) {
      abort(404);
    }

    return view('cliente.editar-pj')->with('pj', $pj)->with('ufs', $ufs);
  }

  /* Responde com um formulário HTML para inserção dos dados
     de um novo cliente (pessoa física ou jurídica). */
  public function apresenta_cadastro(){
    return view('cliente.cadastrar-cliente');
  }

  /* Armazena os dados recebidos do cadastro de um novo cliente
     (pessoa física ou jurídica). */
  public function cadastra(Request $request){
    $dados = $request->all();

    if ($dados['tipo'] == 'PF'){
      $pf = new Pf;
      $pf->cpf = $dados['cpf'];
      $pf->nascimento = $dados['nascimento'];
      $pf->nome = $dados['nome'];
      $pf->sobrenome = $dados['sobrenome'];
      $pf->numero = $dados['numero'];
      $pf->complemento = $dados['complemento'];
      $pf->cep = $dados['cep'];
      $pf->logradouro = $dados['logradouro'];
      $pf->bairro = $dados['bairro'];
      $pf->cidade = $dados['cidade'];
      $pf->uf = $dados['uf'];

      try {
        $pf->save();
      }
      catch(\Illuminate\Database\QueryException $e){
        abort(500);
      }

      $nome = $dados['nome'] . ' ' . $dados['sobrenome'];
    }
    elseif ($dados['tipo'] == 'PJ'){
      $pj = new Pj;
      $pj->cnpj = $dados['cnpj'];
      $pj->razao = $dados['razao'];
      $pj->fantasia = $dados['fantasia'];
      $pj->numero = $dados['numero'];
      $pj->complemento = $dados['complemento'];
      $pj->cep = $dados['cep'];
      $pj->logradouro = $dados['logradouro'];
      $pj->bairro = $dados['bairro'];
      $pj->cidade = $dados['cidade'];
      $pj->uf = $dados['uf'];

      try {
        $pj->save();
      }
      catch(\Illuminate\Database\QueryException $e){
        abort(500);
      }

      $nome = $dados['fantasia'];
    }
    else {
      abort(500);
    }

    return redirect('/clientes/listar?nc=ok&cliente='.$nome);
  }

  /* Lista em formato JSON todos os clientes (pessoas
     físicas e jurídicas) registrados no banco de dados. */
  public function lista_todos_json(){
    $pfs = DB::table('pfs')
               ->select('cpf', 'nascimento as data_de_nascimento', 'nome', 'sobrenome',
                        'cep', 'logradouro', 'numero', 'complemento',
                        'bairro', 'cidade', 'uf')
               ->get()->toArray();

    $pjs = DB::table('pjs')
               ->select('cnpj', 'razao as razao_social', 'fantasia as nome_fantasia',
                        'cep', 'logradouro', 'numero', 'complemento',
                        'bairro', 'cidade', 'uf')
                ->get()->toArray();

    return response()->json(array('status' => 'success',
                                  'data' => array('pessoas_fisicas' => $pfs,
                                                  'pessoas_juridicas' => $pjs)));
  }

  /* Armazena os dados recebidos da alteração de um cliente
     pessoa jurídica específico (id). */
  public function edita_pj(Request $request, $id){
    $dados = $request->all();
    $pj = Pj::find($id);

    if (empty($pj)) {
      abort(500);
    }

    $pj->razao = $dados['razao'];
    $pj->fantasia = $dados['fantasia'];
    $pj->numero = $dados['numero'];
    $pj->complemento = $dados['complemento'];
    $pj->cep = $dados['cep'];
    $pj->logradouro = $dados['logradouro'];
    $pj->bairro = $dados['bairro'];
    $pj->cidade = $dados['cidade'];
    $pj->uf = $dados['uf'];
    $pj->save();

    return redirect('/clientes/pj/'.$id.'?ec=ok&cliente='.$dados['fantasia']);
  }

  /* Armazena os dados recebidos da alteração de um cliente
     pessoa física específico (id). */
  public function edita_pf(Request $request, $id){
    $dados = $request->all();
    $pf = Pf::find($id);

    if (empty($pf)) {
      abort(500);
    }

    $pf->nascimento = $dados['nascimento'];
    $pf->nome = $dados['nome'];
    $pf->sobrenome = $dados['sobrenome'];
    $pf->numero = $dados['numero'];
    $pf->complemento = $dados['complemento'];
    $pf->cep = $dados['cep'];
    $pf->logradouro = $dados['logradouro'];
    $pf->bairro = $dados['bairro'];
    $pf->cidade = $dados['cidade'];
    $pf->uf = $dados['uf'];
    $pf->save();

    return redirect('/clientes/pf/'.$id.'?ec=ok&cliente='.$dados['nome'].' '.$dados['sobrenome']);
  }

  /* Exclui o registro de um cliente (pessoa física
     ou jurídica) específica (id). */
  public function exclui(Request $request){
    $dados = $request->all();

    if ($dados['tipo'] == 'PF') {
      $cliente = Pf::find($dados['id']);
      $nome = $cliente->nome . ' ' . $cliente->sobrenome;
    }
    elseif ($dados['tipo'] == 'PJ') {
      $cliente = Pj::find($dados['id']);
      $nome = $cliente->fantasia;
    }
    else {
      abort(500);
    }

    if (empty($cliente)) {
      abort(500);
    }

    $cliente->delete();
    return response()->json(array('status' => 'success',
                                  'data' => array('url' => '/clientes/listar?dc=ok&cliente='.$nome)));
  }

  /* Responde em formato JSON se um CPF/CNPJ já está
     registrado ou não. */
  public function verifica_conflito(Request $request){
    $dados = $request->all();
    if ($dados['tipo'] == 'PF') {
      $cliente = DB::table('pfs')->where('cpf', '=', $dados['cliente'])->first();
    }
    elseif ($dados['tipo'] == 'PJ') {
      $cliente = DB::table('pjs')->where('cnpj', '=', $dados['cliente'])->first();
    }
    else {
      abort(500);
    }

    return response()->json(array('status' => 'success',
                                  'data' => array('ok' => empty($cliente))));
  }
}
